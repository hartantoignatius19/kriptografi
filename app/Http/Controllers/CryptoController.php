<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CryptoController extends Controller
{
    public function index(){
        return view('form');
    }

    public function encrypt(Request $request){
        $data=$request->all();

        $string_array=stringToArray($data['plaintext']);
        $key_array=stringToArray($data['encryptKey']);
        $data['string_array']=$string_array;

        $generate_key=$this->generate_key($key_array,$string_array);
        $data['key_array']=$generate_key['key'];
        $data['key_ascii_array']=$generate_key['key_ascii'];

        foreach($string_array as $key => $row){
            $data['string_ascii'][]=charToAscii($row);
            $encrypt=$data['string_ascii'][$key] ^ $data['key_ascii_array'][$key];
            $data['encrypt_ascii_array'][]=$encrypt;
            $data['encrypt_array'][]=chr($encrypt);
        }

        $data['cypher_text']=implode($data['encrypt_array']);

        return view('form',$data);
    }


    public function decrypt (Request $request){
        $data=$request->all();
        $chyper_array=stringToArray($data['cyphertext']);
        $key_array=stringToArray($data['decrypt_key']);

        $generate_key=$this->generate_key($key_array,$chyper_array);

        $data['chyper_array']=$chyper_array;
        $data['key_array']=$generate_key['key'];
        $data['key_ascii_array']=$generate_key['key_ascii'];


        foreach($chyper_array as $key=>$row){
            $data['chyper_ascii'][]=charToAscii($row);
            $decrypt=$data['chyper_ascii'][$key] ^ $data['key_ascii_array'][$key];
            $data['decrypt_ascii_array'][]=$decrypt;
            $data['decrypt_array'][]= chr($decrypt);
        }

        $data['decrypt_text']=implode($data['decrypt_array']);
        $data['decrypt_text_ascii']=implode( $data['decrypt_ascii_array']);

        return view('form',$data);
    }

    public function generate_key($key, $string){
        $key_length=count($key);
        $string_length=count($string);

        $data['key_length']=$key_length;

        foreach($key as $index =>$row){
            $key_ascii[$index]=charToAscii($row);
        }

        for($i=$key_length;$i<$string_length;$i++){
            $x=$key[$i-$key_length];
            $x_position=$this->getThePosition($x);

            $y=$key[$i-1];
            $y_position=$this->getThePosition($y);

            $key_ascii[$i]=($x_position + $y_position)+64;
            $key_ascii[$i]=$this->convertForNotBiggerThan127($key_ascii[$i]);

            $key[$i]=chr($key_ascii[$i]);

        }

        $data['key_ascii']=$key_ascii;
        $data['key']=$key;
        return $data;
    }

    public function getThePosition($char){
        $position=charToAscii(ucfirst($char))-64;

        return $position;
    }

    public function convertForNotBiggerThan127($key_ascii){
        $loop=1;
        do{
            if($key_ascii>127)
            {
                $key_ascii=$key_ascii-63;
            }
            else{
                $loop=0;
            }
        }while($loop==1);

        return $key_ascii;
    }

}
