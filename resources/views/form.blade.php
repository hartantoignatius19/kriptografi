<!doctype html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <style>
        .column {
            width: 50% ;
            float: left;
        }
        .row:after {
            display: table;
            clear: both;
        }
        #customers td, #customers th {
            border: 1px solid #ddd;
            padding: 8px;
        }
        #customers th {
            padding-top: 12px;
            padding-bottom: 12px;
            text-align: left;
            background-color: #4CAF50;
            color: white;
        }
        .header {
            list-style-type: none;
            margin: 0;
            padding: 0;
            overflow: hidden;
            background-color: #3a4f3f;
        }

        .style {
            display: block;
            color: white;
            text-align: center;
            padding: 14px 16px;
            text-decoration: none;
        }
        div {
            background-color: #77a37a;
        }
        .grid-container-item {
            display: grid;
            grid-template-columns: auto auto;
            padding: 10px;
        }
        .grid-item-header {
            font-size: 30px;
            text-align: center;
        }
        .grid-item {
            background-color: #587e60   ;
            font-size: 17px;
            padding-left: 5px;
            border: 1px solid rgba(0, 0, 0, 0.8);
        }
        .fieldStyle {
            width: 97%;
            margin-right: 2px;
        }
        .buttonStyle {
            float: right;
            margin-right: 6px;
        }
        .tableStyle {
            padding-left: 10px;
        }
    </style>
    <title>Cryptography</title>
</head>
<body>
<div class="header">
    <h1 class="style"> Encrypt and Decrpyt Use One Time Pad Xor Algorithms</h1>
</div>
<div class="componentContainer">
    <div class="column">
        <form method="post" action="{{route('encrypt')}}">
            {{csrf_field()}}
            <div class="grid-item-header">
                Encrypt
            </div>
            <div class="grid-container-item">
                <div class="grid-item">
                    <label for="plain-text">
                        Plain Text :
                    </label>
                </div>
                <div class="grid-item">
                    <input name="plaintext" type="text" value="{{isset($plaintext)?$plaintext:null}}" class="fieldStyle"/>
                </div>
                <div class="grid-item">
                    <label for="encryptKey">
                        Key :
                    </label>
                </div>
                <div class="grid-item">
                    <input name="encryptKey" type="text"  value="{{isset($encryptKey)?$encryptKey:null}}" class="fieldStyle"/>
                </div>
                <div class="grid-item">
                </div>
                <div class="grid-item">
                    <input type="submit" value="Encrpyt" class="buttonStyle">
                </div>
                <div class="grid-item">
                    <label for="encryptKey">
                        Chyper Text :
                    </label>
                </div>
                <div class="grid-item">
                    <input type="text" disabled value=" {{isset($cypher_text)?$cypher_text:null}}" class="fieldStyle"/>
                </div>
            </div>
            <div class="tableStyle">
                <div>
                    <div>
                        Plain Text ASCII:
                        <div>
                            <table id="customers">
                                <tr>
                                    <td>No</td>
                                    <td>Plain text</td>
                                    <td>Ascii</td>
                                </tr>
                                @if(isset($string_array))

                                    @foreach($string_array as $key =>$row)
                                        <tr>
                                            <td>{{$key+1}}</td>
                                            <td>{{$row}}</td>
                                            <td>{{$string_ascii[$key]}}</td>
                                        </tr>
                                    @endforeach
                                @else
                                @endif
                            </table>

                        </div>
                    </div>

                    <div>
                        Key ASCII:
                        <div>
                            <table id="customers">
                                <tr>
                                    <td>No</td>
                                    <td>Key</td>
                                    <td>Ascii</td>
                                </tr>
                                @if(isset($key_array))

                                    @foreach($key_array as $key =>$row)
                                        <tr>
                                            <td>{{$key+1}}</td>
                                            <td>{{$row}}</td>
                                            <td>{{$key_ascii_array[$key]}}</td>
                                        </tr>
                                    @endforeach
                                @endif
                            </table>

                        </div>
                    </div>


                    <div>
                        XOR Plain Text With Key And Convert To String
                        <div>
                            <table id="customers">
                                <tr>
                                    <td>No</td>
                                    <td>Plain Text</td>
                                    <td>Key</td>
                                    <td>Plain Text Ascii</td>
                                    <td>Key Ascii</td>
                                    <td>Cyper Text Ascii</td>
                                    <td>Cyper Text</td>
                                </tr>
                                @if(isset($encrypt_ascii_array))

                                    @foreach($encrypt_ascii_array as $key =>$row)
                                        <tr>
                                            <td>{{$key+1}}</td>
                                            <td>{{$string_array[$key]}}</td>
                                            <td>{{$key_array[$key]}}</td>
                                            <td>{{$string_ascii[$key]}}</td>
                                            <td>{{$key_ascii_array[$key]}}</td>
                                            <td>{{$encrypt_ascii_array[$key]}}</td>
                                            <td>{{$encrypt_array[$key]}}</td>
                                        </tr>
                                    @endforeach
                                @endif
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </form>
    </div>
    <div class="column">

        <form method="post" action="{{route('decrypt')}}">
            {{csrf_field()}}
            <div class="grid-item-header">
                Decrypt
            </div>
            <div class="grid-container-item">
                <div class="grid-item">
                    <label for="plain-text">
                        Chyper Text :
                    </label>
                </div>
                <div class="grid-item">
                    <input name="cyphertext" type="text" value="{{isset($cyphertext)?$cyphertext:null}}" class="fieldStyle"/>
                </div>
                <div class="grid-item">
                    <label for="encryptKey">
                        Key :
                    </label>
                </div>
                <div class="grid-item">
                    <input name="decrypt_key" class="fieldStyle" type="text"  value="{{isset($decrypt_key)?$decrypt_key:null}}"/>
                </div>
                <div class="grid-item">
                </div>
                <div class="grid-item">
                    <input type="submit" value="Decrypt" class="buttonStyle">
                </div>
                <div class="grid-item">
                    <label for="encryptKey">
                        Plain Text :
                    </label>
                </div>
                <div class="grid-item">
                    <input type="text" disabled value=" {{isset($decrypt_text)?$decrypt_text:null}}" class="fieldStyle"/>
                </div>
            </div>
            <div class="tableStyle">
                <div>
                    Chyper Text ascii:
                    <table id="customers">
                        <tr>
                            <td>No</td>
                            <td>Key</td>
                            <td>Ascii</td>
                        </tr>
                        @if(isset($chyper_array))
                            @foreach($chyper_array as $key =>$row)
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td>{{$row}}</td>
                                    <td>{{$chyper_ascii[$key]}}</td>
                                </tr>
                            @endforeach
                        @endif
                    </table>
                </div>
                <div>
                    Key ASCII:
                    <table id="customers">
                        <tr>
                            <td>No</td>
                            <td>Key</td>
                            <td>Ascii</td>
                        </tr>
                        @if(isset($key_array))
                            @foreach($key_array as $key =>$row)
                                <tr>
                                    <td>{{$key+1}}</td>
                                    <td>{{$row}}</td>
                                    <td>{{$key_ascii_array[$key]}}</td>
                                </tr>
                            @endforeach
                        @endif
                    </table>
                </div>

                <div>
                    XOR Cyper Text With Key And Convert To String
                    <div>
                        <table id="customers">
                            <tr>
                                <td>No</td>
                                <td>Cyper Text</td>
                                <td>Key</td>
                                <td>Cyper Text Ascii</td>
                                <td>Key Ascii</td>
                                <td>Plain Text Ascii</td>
                                <td>Plain Text</td>
                            </tr>
                            @if(isset($decrypt_ascii_array))
                                @foreach($decrypt_ascii_array as $key =>$row)
                                    <tr>
                                        <td>{{$key+1}}</td>
                                        <td>{{$chyper_array[$key]}}</td>
                                        <td>{{$key_array[$key]}}</td>
                                        <td>{{$chyper_ascii[$key]}}</td>
                                        <td>{{$key_ascii_array[$key]}}</td>
                                        <td>{{$decrypt_array[$key]}}</td>
                                        <td>{{$decrypt_ascii_array[$key]}}</td>
                                    </tr>
                                @endforeach
                            @endif
                        </table>
                    </div>

                </div>
            </div>
        </form>
    </div>
</div>
</body>
</html>
